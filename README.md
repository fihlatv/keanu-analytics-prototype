# keanu-analytics

> privacy respecting analytics for your matrix homeserver deployment


## On Metrics

The goal of keanu-analytics (KA) is to collect just enough data to understand if
people are using keanu and how their usage is evolving while maintaining privacy
and minimizing collected data.

KA is built to provide data that can answer the following questions:

1. User engagement: How many people sent a message for the first time ever this period?
   
   This metric is about user engagement. We want users to complete the core
   action of sending a message after signing up. We want to know how many users
   completed this action per reporting period.
   
2. Retention: How many group rooms had more than 1 message sent during the period?

   Usage of group rooms is considered an important retention metric.
   
3. Retention: How many media files were uploaded?

   Sending media through the system is also considered an important retention metric.

All the above metrics will be reported on based on geo-buckets. A geo-bucket is
a geographical region that a user falls into. Which geo-buckets used are left up
to the admins of KA. One could for example have three geo buckets: Western USA,
The Netherlands, and Other. In this case, the above metrics would be reported on
these three buckets. The Other bucket is always included as a catch-all.

### Data Model and Privacy Concerns

We aim to provide these metrics while minimizing the amount of data that needs
to be collected. In particular we want to avoid tracking any data on a per-user
basis.


#### Threat Model

keanu-analytics aims to protect against:

1. Data leaks - in the case that KA's database alone is leaked, we want to ensure no
   individual user is identifiable in the dataset.
   
2. Legal data requests - in the case that KA's operator's are required by law to
   provide data, we want to ensure no individual user is identifiable in the
   dataset.
   
KA should contain such little data as to make it unappealing as a target for
adversaries.

keanu-analytics does not protect against:

1. Attacks against the matrix homeserver or other infrastructure
2. Interception by a Global Passive Adversary or rogue server administrator

This is not to say these are no important threats to mitigate, but that is what
KA itself is concerned with.

#### Data: Core events

Like traditional analytics systems, KA stores its events in a singular table
where each row is a unique event. However unlike traditional analytic systems,
the data that composes an event is non-identifying and non-granular. 

We do not use specific timestamps nor unique ids for events. Rather than a
timestamp we use the notion of a reporting period. The reporting period is the
most granular slice of time used. By default the keanu-analytics reporting
period is a week.

* `period`: the reporting period, e.g., `2019-W1` for the first week of 2019
* `event`: the type of event, one of: `message`, `upload`
* `bucket`: the geo bucket the user who initiated the event falls in, e.g.,
  `netherlands` or `other`
* `is_user_engagement_period`: a boolean field that indicates whether this event was the
  first time a user engaged with keanu this period
* `is_user_engagement_first`: a boolean field that indicates whether this event was the
  first time a user engaged with keanu *ever*
* `is_federated`: a boolean field that indicates whether this event was sent to a
  federated homeserver (we only track events originating from our homeserver,
  never inbound events)
* `is_group`: a boolean field that indicates whether this event was sent to a
  room that is considered a group
* `is_group_engagement`: a boolean field that indicates whether this event was
  the first sent to a room considered a group for this period

#### Data: User engagement

In order to determine if a user engaged with keanu, that is, sent a message at
least once, we use a bloom filter.

KA stores [a bloom filter](https://llimllib.github.io/bloomfilter-tutorial/) of
hashed `mxids`. A bloom filter is used as a soft measure to reduce privacy
concerns. 

What other techniques were considered?

1. single column database table - KA could simply store a table of mxids that
   have engaged, but underlying implementation details ([ctids in
   postgres](https://www.postgresql.org/docs/10/ddl-system-columns.html)) would
   reveal approximately (or at least relatively) when the mxid was added to the
   table.

2. a single array-as-a-set value: Postgresql has no set datatype, and [kludging
   one with the existing array
   functions](https://stackoverflow.com/questions/24987547/is-there-a-standard-approach-for-dealing-with-unordered-arrays-sets-in-postgre)
   is very hacky.

More importantly however, these two alternate techniques require persisting in
the keanu-analytics database personally identifiable information about the user
(their mxid). We must avoid this.
 
Using a bloom filter gives us ["better than nothing
privacy"](https://doi.org/10.1007/978-3-642-33627-0_27): the list of users is
not explicitly enumerated in KA's datastore.

Hopefully someday it will be impossible or unnecessary to track this data point
from the server altogether.

#### Data: Group Rooms

In matrix there is no concept of private-chats and group rooms like there was in
XMPP. There is only the concept of rooms. Our metrics however want to capture
how many groups are active throughout a period. For our purposes a "group" is a
matrix room with more than two members.

Membership is only considered at the time of the first message in the reporting
period. Active rooms are rooms where at least one message was sent during the period.

Obviously a group should only be counted as active once during a reporting
period, this entails KA storing the fact that a group was already counted. This
is the same issue as tracking initial user engagement, so the same solution is
applied.

* For each reporting period a bloom filter is created. 
* When a message send event is received for a room, the number of members is
 fetched from the homeserver. If the number is less than 2, the event is discarded.
* Otherwise if the number is more than 2, the room id is checked against the bloom filter
* If the room id is contained in the bloom filter, the event is discarded
* Otherwise, the room id is added to the bloom filter and the `active_groups`
  tally for the `period` incremented
* At the end of the reporting period, the bloom filter is discarded
 
#### Bloom Filter Implementation

We use a standard bloom filter implementation. The hash functions are
[SipHash](https://131002.net/siphash/) with two different long-lived secret
keys. We have used the technique from [Kirsch & Mitzenmacher][kirsch] to
build total `k` hash functions as a combination of two hash functions.

* Bloom filter implementation
  [clj-bloom](https://github.com/kyleburton/clj-bloom) by Kyle Burton (EPL
  1.0) - limitation of 2^32-1 max items
* SipHash implementation provided by
  [libsodium](https://download.libsodium.org/doc/hashing/short-input_hashing)
  via [caesium clojure wrapper](https://github.com/lvh/caesium**
  
**Further Reading**

[Thomas Gerbet, Amrit Kumar, Cédric Lauradoux. The Power of Evil Choices in Bloom Filters. RR-8627, INRIA Grenoble. 2014. hal-01082158v2][gerbet]

[gerbet]: https://hal.inria.fr/hal-01082158v2/document

[Niedermeyer, Frank & Steinmetzer, Simone & Kroll, Martin & Schnell, Rainer. (2014). Cryptanalysis of Basic Bloom Filters Used for Privacy Preserving Record
Linkage. Journal of Privacy and Confidentiality. 6. 10.29012/jpc.v6i2.640.][niedermeyer]

[niedermeyer]: https://pdfs.semanticscholar.org/1ad4/f6f95198bdbd13fd9a0be6d66f81fc018ac2.pdf


[Bianchi, G., Bracciale, L., & Loreti, P. (2012). "Better Than Nothing" Privacy with Bloom Filters: To What Extent? Privacy in Statistical Databases. DOI:10.1007/978-3-642-33627-0_27][bianchi]

[bianchi]: https://www.researchgate.net/publication/262401813_Better_Than_Nothing_Privacy_with_Bloom_Filters_To_What_Extent

[Kirsch, Adam & Mitzenmacher, Michael. (2006). Less Hashing, Same Performance: Building a Better Bloom Filter.. 4168. 456-467.][kirsch]

[kirsch]: https://www.eecs.harvard.edu/~michaelm/postscripts/rsa2008.pdf



## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen



## Running

Don't forget to configure your `dev-config.edn`
To start a web server for the application, run:

    lein run 

## License

Unless otherwise noted, all files © 2019 Abel Luck and The Guardian Project

Distributed under the terms of the GNU Affero General Public License (AGPL)
v3.0 or any later version, with the EPL v1.0 exception.

See [LICENSE](/LICENSE) for details and EPL v1.0 exception.

This project was generated using Luminus version "3.48" with

```console
$ lein new luminus keanu-analytics +service +postgres
```


