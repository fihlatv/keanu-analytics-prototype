(ns keanu-analytics.memdb
  (:require
   [clojure.tools.logging :as log]
   [keanu-analytics.config :refer [env]]
   [mount.core :refer [defstate]]))

(defn init-memdb []
  {::user-engage-first #{}
   ::user-engage-period  #{}
   ::group-engage-period #{}})

(defstate ^:dynamic store
  :start (atom (init-memdb)))

(defn clear! [k]
  (swap! store #(assoc % k #{})))

(defn reset-memdb! []
  (reset! store (init-memdb)))

(def period-atom (atom nil))

(defn set-period! [period]
  (when (not= period @period-atom)
    (println "NEW PERIOD: from " @period-atom " to " period)
    (clear! ::user-engage-period)
    (clear! ::group-engage-period)
    (reset! period-atom period)))

(defn included? [k v]
  (contains? (get @store k) v))

(defn add-to-set [s v]
  (conj s v))

(defn add! [k v]
  (swap! store #(update % k add-to-set v)))
