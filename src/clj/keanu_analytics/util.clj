(ns keanu-analytics.util)

(defn fmap
  "Applies function f to each value of a map and returns the updated map

   Copyright (c) Konrad Hinsen, 2009-2011. All rights reserved.  The use
   and distribution terms for this software are covered by the Eclipse
   Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
   which can be found in the file epl-v10.html at the root of this
   distribution.  By using this software in any fashion, you are
   agreeing to be bound by the terms of this license.  You must not
   remove this notice, or any other, from this software.

  ref: https://github.com/clojure/algo.generic/blob/master/src/main/clojure/clojure/algo/generic/functor.clj#L33"
  [f m]
  (into (empty m) (for [[k v] m] [k (f v)])))

(defn fmapk
  "Like fmap but f is arity 2, taking k and v"
  [f m]
  (into (empty m) (for [[k v] m] [k (f k v)])))
