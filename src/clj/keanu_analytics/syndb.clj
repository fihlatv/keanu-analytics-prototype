(ns keanu-analytics.syndb
  (:require
   [clojure.java.jdbc :as jdbc]
   [clojure.tools.logging :as log]
   [conman.core :as conman]
   [keanu-analytics.config :refer [env]]
   [mount.core :refer [defstate]]))

(defn log-sqlvec [sqlvec]
  (log/info (->> sqlvec
                 (map #(clojure.string/replace (or % "") #"\n" ""))
                 (clojure.string/join " ; "))))

(comment
  (defn log-command-fn [this db sqlvec options]
    (log-sqlvec sqlvec)
    (condp contains? (:command options)
      #{:!} (hugsql.adapter/execute this db sqlvec options)
      #{:? :<!} (hugsql.adapter/query this db sqlvec options)))

  (defmethod hugsql.core/hugsql-command-fn :! [sym] `log-command-fn)
  (defmethod hugsql.core/hugsql-command-fn :<! [sym] `log-command-fn)
  (defmethod hugsql.core/hugsql-command-fn :? [sym] `log-command-fn))

(defstate ^:dynamic *synapse*
  :start (if-let [jdbc-url (env :synapse-database-url)]
           (conman/connect! {:jdbc-url jdbc-url})
           (do
             (log/warn "database connection URL was not found, please set :database-url in your config, e.g: dev-config.edn")
             *synapse*))
  :stop (conman/disconnect! *synapse*))

(conman/bind-connection *synapse* "sql/synapse-queries.sql")

(defn get-device-name [device-id]
  (:display_name (first (lookup-device-name *synapse* {:device-id device-id}))))

(defn get-devices [user-id]
  (lookup-devices *synapse* {:user-id user-id}))

(defn count-joined-members
  [room-id]
  (or
   (:count (first (_count-joined-members *synapse* {:room-id room-id})))
   0))
