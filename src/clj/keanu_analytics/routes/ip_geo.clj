(ns keanu-analytics.routes.ip-geo
  (:require
   [clojure.tools.logging :as log]
   [mount.core :as mount]
   [keanu-analytics.config :refer [env]]
   [geo [io :as gio] [jts :as jts]]
   [ip-geoloc.core :as geoip]
   [ip-geoloc-contains.core :as geoipc])
  (:use [slingshot.slingshot :only [throw+ try+]])
  )

(mount/defstate ip-geo
  :start {:buckets (geoipc/prepare-buckets (:geo-buckets env))
          :provider (geoip/start-provider {:database-file (:mmdb-path env)})})

(defn ip->geo
  "Given an `ip`, performs a lookup using the `provider` and returns the lookup result."
  [ip]
  ((memoize geoip/geo-lookup) (:provider ip-geo) ip))

(defn ip-to-geo-bucket-resolver
  "Returns  a function that maps an ip address to one of the `buckets`, or `default-bucket`."
  ([default-bucket]
   (ip-to-geo-bucket-resolver (:buckets ip-geo) default-bucket))
  ([buckets default-bucket]
   (fn [ip]
     (try
      (let [geo (ip->geo ip)]
        (geoipc/match-geo geo buckets default-bucket))

      (catch java.net.UnknownHostException _
        (log/warn "Failed to resolve an ip")
        (log/debug (str "Failed to resolve ip: '" ip "'"))
        default-bucket)))))
