(ns keanu-analytics.report
  (:require    [cheshire.core :refer [generate-string parse-string]]
               [clojure.java.jdbc :as jdbc]
               [clojure.tools.logging :as log]
               [conman.core :as conman]
               [java-time :as jt]
               [clojure.java.io :as io]
               [clojure.data.csv :as csv]

               [keanu-analytics.db.core :as db]
               [keanu-analytics.config :refer [env]]
               [keanu-analytics.cal :as cal]
               [keanu-analytics.ingest :refer [date-range]]))

;(def start-date (cal/from-period [2019 1]))
;(def end-date (cal/from-period [2020 1]))
;; (def periods (map cal/period-for (date-range start-date end-date)))
;(def periods (date-range start-date end-date))
(def buckets ["other" "tibet" "china"])

(defn get-count [q]
  (:count (first q)))

(defn report-period-bucket [period ]
  (mapv vals (db/count-user-engagements-app {:period  period})))

  ;[(mapv vals (db/count-user-engagements {:period  period :bucket bucket}))
   ;(mapv vals (db/count-user-engagements-period {:period  period :bucket bucket}))])

(defn report-period [period ]
  (mapv vals (db/count-group-engagements {:period  period})))

(defn report-period-old [period]
  [(get-count (db/count-group-engagements {:period period}))
   (get-count (db/count-private-messages {:period period}))
   (get-count (db/count-group-messages {:period period}))])

(defn report-period-bucket-platform [period bucket platform]
  [(get-count (db/count-user-engagements-platform {:period  period :bucket bucket :platform platform}))
   (get-count (db/count-user-engagements-period-platform {:period  period :bucket bucket :platform platform}))])

(defn report-period-platform [period platform]
  [(get-count (db/count-group-engagements-platform {:period period :platform platform}))
   (get-count (db/count-private-messages-platform {:period period :platform platform}))
   (get-count (db/count-group-messages-platform {:period period :platform platform}))])

(defn write-report [report name header]
  (with-open [writer (io/writer name)]
    (csv/write-csv writer
                   (concat
                    [header]
                    report))))

(defn munge-result [func periods]
  (apply concat (for [period periods]
                   (let [period-str (cal/period-for period)
                         date-str (str (cal/adate->local-date period))]
                     ( map #(concat  [period-str date-str] %)
                              (func  period-str))))
  ))
(defn report [periods]
  (write-report  (munge-result report-period-bucket periods)
                 "/home/admin/report-user-engagement.csv"
                 ["period" "date" "appname" "platform" "bucket" "users engaged (first ever)" "users engaged (per period)"])

  (write-report  (munge-result report-period periods)
                 "/home/admin/report-period.csv"
                 ["period" "date"  "groups engaged (per period)" "private messages sent (total)" "group messages sent (total)"]))

