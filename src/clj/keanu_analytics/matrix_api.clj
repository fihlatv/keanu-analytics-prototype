(ns keanu-analytics.matrix-api
  (:require [clojure.tools.logging :as log]
            [cheshire.core :as json]
            [mount.core :as mount]
            [clj-http.client :as http]
            [keanu-analytics.config :refer [env]]
            [reitit.coercion :as coercion]
            [reitit.coercion.spec :as spec-coercion]
            [reitit.core :as r])
  (:use [slingshot.slingshot :only [throw+ try+]]))

(defn wrap-auth [request token]
  (assoc-in request [:headers "Authorization"] (str "Bearer " token)))

(defn wrap-endpoint [request hs-base]
  (-> request
      (update :url #(apply format (str hs-base %) (:url-params request)))
      (assoc :insecure? false)
      (assoc :cookie-policy :standard)  ;; ref https://github.com/dakrone/clj-http/issues/325
      ))

(defn wrap-json [request]
  (let [has-body?     (contains? request :body)
        body-request? (contains? #{:post :put} (:method request))]
    ;; (println has-body? body-request?)
    (cond-> request
      true (assoc-in [:headers "Content-Type"] "application/json; charset=utf-8")
      (and body-request? has-body?) (update :body json/generate-string))))

(defn log [_]
  (println _) _)

(defn client
  [hs-base token http]
  (fn [request]
    (-> request
        (wrap-auth token)
        (wrap-endpoint hs-base)
        ;; log
        wrap-json
        http)))

(defn parse-response [response]
  (cond-> response
    (string? (:body response))
    (update :body json/parse-string keyword)
    true
    (select-keys [:status :body :error])))

(defn ->data [response]
  (get-in response [:body]))

(defn fetch [client request]
  (->data (parse-response (client request))))

(defn get-devices
  ([user-id]
   {:method :get
    :url "/_matrix/client/r0/devices"
    :query-params (when-some [u user-id] {:user_id u})})
  ([client user-id]
   (fetch client (get-devices user-id))))

(defn get-joined-members
  ([user-id room-id]
   {:method :get
    :url "/_matrix/client/r0/rooms/%s/joined_members"
    :url-params [room-id]
    :query-params (when-some [u user-id] {:user_id u})})
  ([client user-id room-id]
   (fetch client (get-joined-members user-id room-id))))

(defn count-joined-members
  [client user-id room-id]
  (try+
   (count (:joined (get-joined-members client user-id room-id)))
   (catch [:status 403] {:keys [request-time headers body]}
     (log/warn "count-joined-members got 403 response, assuming 0 members")
     (log/debug (str "count-joined-members got 403 response for:  " room-id))
     0)
   (catch Object _
     (log/error (:throwable &throw-context) "unexpected error")
     (throw+))))

(defn init-app-server-client [{:keys [hs-url hs-token]}]
  (client (str "https://" hs-url) hs-token http/request))

(mount/defstate matrix-client
  :start (init-app-server-client env))
