CREATE SCHEMA atomic;
--;;
CREATE TABLE atomic.events (
  period                     TEXT,
  event                      TEXT,
  bucket                     TEXT,
  is_user_engagement_period  BOOLEAN,
  is_user_engagement_first   BOOLEAN,
  is_federated               BOOLEAN,
  is_group                   BOOLEAN,
  is_group_engagement_period BOOLEAN
);
--;;
CREATE INDEX period_idx
  ON atomic.events (period);
--;;
CREATE INDEX event_idx
  ON atomic.events (event);
--;;
CREATE INDEX bucket_idx
  ON atomic.events (bucket);
--;;
CREATE TABLE atomic.blooms (
  id    TEXT PRIMARY KEY,
  bytes bytea
);
