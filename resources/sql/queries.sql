-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(id, first_name, last_name, email, pass)
VALUES (:id, :first_name, :last_name, :email, :pass)

-- :name update-user! :! :n
-- :doc updates an existing user record
UPDATE users
SET first_name = :first_name, last_name = :last_name, email = :email
WHERE id = :id

-- :name get-user :? :1
-- :doc retrieves a user record given the id
SELECT * FROM users
WHERE id = :id

-- :name delete-user! :! :n
-- :doc deletes a user record given the id
DELETE FROM users
WHERE id = :id


-- :name get-bloom :? :1
-- :doc retrieves the bloom fitler of id
SELECT * FROM atomic.blooms
WHERE id = :id

-- :name delete-bloom! :! :n
-- :doc deletes a bloom given the id
DELETE FROM atomic.blooms
WHERE id = :id

-- :name upsert-bloom! :! :n
-- :doc upserts a bloom filter
INSERT INTO atomic.blooms
(id, bytes)
VALUES (:id, :bytes)
ON CONFLICT (id)
DO UPDATE SET
bytes = EXCLUDED.bytes

-- :name create-bloom! :! :n
-- :doc creates a new bloom filter
INSERT INTO atomic.blooms
(id, bytes)
VALUES (:id, :bytes)

-- :name create-event! :! :n
-- :doc creates a new event
INSERT INTO atomic.events
(period,
 event,
 bucket,
 is_user_engagement_period,
 is_user_engagement_first,
 is_federated,
 is_group,
 is_group_engagement_period,
 platform,
 appname)
VALUES (
  :period,
  :event,
  :bucket,
  :is_user_engagement_period,
  :is_user_engagement_first,
  :is_federated,
  :is_group,
  :is_group_engagement_period,
  :platform,
  :appname)

-- :name count-user-engagements-app :1
-- :doc Count the message events, see get-message-events
SELECT e.appname,
e.platform,
e.bucket,
count (1) filter (where e.is_user_engagement_first is TRUE) as engaged_first,
count (1) filter (where e.is_user_engagement_first is FALSE and e.is_user_engagement_period is TRUE) as engaged_period

FROM atomic.events e
WHERE
    e.period = :period group by 1, 2, 3;

-- :name count-user-engagements :1
-- :doc Count the message events, see get-message-events
SELECT e.appname, count(e.period) FROM atomic.events e
WHERE
    e.is_user_engagement_first is TRUE AND
    e.bucket = :bucket AND
    e.period = :period
    group by 1

-- :name count-user-engagements-period :1
-- :doc Count the message events, see get-message-events
SELECT e.appname, count(e.period) FROM atomic.events e
WHERE
    e.is_user_engagement_period is TRUE AND
    e.is_user_engagement_first is FALSE AND
    e.bucket = :bucket AND
    e.period = :period
    group by 1

-- :name count-group-engagements :1
-- :doc Count the number of groups that sent at least one message in a period
SELECT
count(1) filter (where e.is_group_engagement_period IS TRUE) as groups_engaged,
count(1) filter (where e.is_group is FALSE) as private_messages,
count(1) filter (where e.is_group is TRUE) as group_messages
FROM atomic.events e
WHERE e.is_group_engagement_period IS TRUE
  AND e.period = :period

-- :name count-private-messages :1
-- :doc Count the number of private messages in a period
SELECT count(*)
FROM atomic.events e
WHERE e.is_group IS FALSE
  AND e.period = :period

-- :name count-group-messages :1
-- :doc Count the number of group messages in a period
SELECT count(*)
FROM atomic.events e
WHERE e.is_group IS TRUE
  AND e.period = :period

--
-- :name count-user-engagements-platform :1
-- :doc Count the message events, see get-message-events
SELECT count(e.period) FROM atomic.events e
WHERE
    e.is_user_engagement_first is TRUE AND
    e.bucket = :bucket AND
    e.period = :period AND
    e.platform = :platform

-- :name count-user-engagements-period-platform :1
-- :doc Count the message events, see get-message-events
SELECT count(e.period) FROM atomic.events e
WHERE
    e.is_user_engagement_period is TRUE AND
    e.is_user_engagement_first is FALSE AND
    e.bucket = :bucket AND
    e.period = :period AND
    e.platform = :platform


-- :name count-group-engagements-platform :1
-- :doc Count the number of groups that sent at least one message in a period
SELECT count(*)
FROM atomic.events e
WHERE e.is_group_engagement_period IS TRUE
  AND e.period = :period AND
    e.platform = :platform

-- :name count-private-messages-platform :1
-- :doc Count the number of private messages in a period
SELECT count(*)
FROM atomic.events e
WHERE e.is_group IS FALSE
  AND e.period = :period AND
    e.platform = :platform

-- :name count-group-messages-platform :1
-- :doc Count the number of group messages in a period
SELECT count(*)
FROM atomic.events e
WHERE e.is_group IS TRUE
  AND e.period = :period AND
    e.platform = :platform
