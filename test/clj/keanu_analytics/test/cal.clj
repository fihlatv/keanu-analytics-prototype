(ns keanu-analytics.test.cal
  (:require [clojure.test :refer :all]
            [keanu-analytics.cal :refer :all]
            [java-time :as jt]))

(deftest test-calendar
  (testing "creation"
    (is (= (adate) (adate (jt/local-date))))
    (is (= (period-str (year-week-of-year (adate 2019 1 1))) "2019-W1"))
    ;; Friday, September 6, 2019 4:36:59 PM is in W26
    (is (= [2019 36] (year-week-of-year (ts->adate 1567787819000))))
    ;; roundtripping through the ts funcs should result in midnight on sept 6
    (is (= 1567728000000 (adate->ts (ts->adate 1567787819000))))
    (is (= [2019 2] (year-week-of-year (next-week (adate 2019 1 1)))))
    (is (= [2020 1] (year-week-of-year (next-week (adate 2019 12 31)))))
    (is (= [2019 1] (year-week-of-year  (from-period [2019 1]))))
    (is (= [2018 52] (year-week-of-year  (from-period [2018 52]))))
    (is (= [2016 52] (year-week-of-year  (from-period [2016 52]))))
    ))
