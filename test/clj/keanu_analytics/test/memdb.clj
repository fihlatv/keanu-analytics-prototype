(ns keanu-analytics.test.memdb
  (:require
   [clojure.test :refer :all]
   [mount.core :as mount]
   [keanu-analytics.memdb :as memdb]))

(use-fixtures :once
  (fn [f]
    (mount/start #'keanu-analytics.memdb/store)
    (f)
    (memdb/reset-memdb!))

  memdb-test-fixture)

(deftest memdbtest
  (testing "memdb"
    (is (not (memdb/included? ::memdb/user-engage-first "foobar")))
    (memdb/add! ::memdb/user-engage-first "foobar")
    (is (memdb/included? ::memdb/user-engage-first "foobar"))
    (memdb/clear! ::memdb/user-engage-first)
    (is (not (memdb/included? ::memdb/user-engage-first "foobar")))

    (memdb/add! ::memdb/user-engage-first "foobar")
    (memdb/add! ::memdb/user-engage-first "barfoo")
    (is (memdb/included? ::memdb/user-engage-first "foobar"))
    (is (memdb/included? ::memdb/user-engage-first "barfoo"))
    (is (not (memdb/included? ::memdb/user-engage-first "nope")))

    (memdb/add! ::memdb/user-engage-period "foobar")
    (memdb/add! ::memdb/user-engage-period "barfoo")
    (memdb/clear! ::memdb/user-engage-first)
    (is (memdb/included? ::memdb/user-engage-period "foobar"))
    (is (memdb/included? ::memdb/user-engage-period "barfoo"))
    (is (not (memdb/included? ::memdb/user-engage-first "foobar")))
    (is (not (memdb/included? ::memdb/user-engage-first "barfoo")))

    ))
(deftest memdb-period-test
  (testing "period"
    (memdb/set-period! "2019-W1")
    (memdb/add! ::memdb/user-engage-first "foobar")
    (memdb/add! ::memdb/user-engage-period "foobar")
    (memdb/add! ::memdb/group-engage-period "foobar")
    (is (memdb/included? ::memdb/user-engage-first "foobar"))
    (is (memdb/included? ::memdb/user-engage-period "foobar"))
    (is (memdb/included? ::memdb/group-engage-period "foobar"))

    (memdb/set-period! "2019-W2")
    (is (memdb/included? ::memdb/user-engage-first "foobar"))
    (is (not (memdb/included? ::memdb/user-engage-period "foobar")))
    (is (not (memdb/included? ::memdb/group-engage-period "foobar")))

    )
  )
