(ns keanu-analytics.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[keanu-analytics started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[keanu-analytics has shut down successfully]=-"))
   :middleware identity})
